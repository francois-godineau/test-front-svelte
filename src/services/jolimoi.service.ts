import axios from 'axios'

const baseApi = `${process.env.BACKEND}/api/jolimoi`

export const arabicToRoman = (decimal: string): any => {
  return axios.post(`${baseApi}/arabic-to-roman`, {
    decimal: parseInt(decimal)
  })
}