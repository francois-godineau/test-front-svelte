# Frontend tests with Svelte + TS + Vite

This template should help get you started developing with Svelte and TypeScript in Vite.

## Description

This repository aims to gather all the frontend technical tests that I had to do for job interviews.

[Svelte](https://svelte.dev/) framework.
[Vite](https://vitejs.dev/guide/) bundler.

## Jolimoi
- [Jolimoi](https://www.jolimoi.com/) technical test, 3h
- [Job offer](https://www.welcometothejungle.com/fr/companies/jolimoi/jobs/dev-front-javascript_paris) Lead dev fullstack
- [Test instructions](./doc)

### attack plan
- init back repo, bootstrap nest, readme : 30'
- init front repo, bootstrap svelte, readme : 30'
- implement step1 back + unit test : 30'
- implement step1 front + unit test : 30'
- implement step2 back + unit test : 30'
- implement step2 front + unit test : 30'
Total : 3h

This raodmap was made on the time constraints imposed by the test.

To be confortable and really have time to write documentation and roadmap, to add best practices, refactoring code for better architecture, add unit tests, add validations, deal with unexpected bugs, configure IDE,  I think 1-3 days (depending on developper experience) would be more realistic.

Tool to enforce time management : [Pomodoro](https://pomofocus.io/app)

#### about my choices
- I chose nest because I wanted to discover it and it provides bootsraper with unit tests, end-to-end test, linter and  native SSE support.
- I chose vite + svelte for frontend because i already know them, they are fast and give a really good developer experience
- I chose to dev in Typescript because I don't really have experience with this language but I'm able to learn it quite quickly. 
- I chose the [ronu](https://github.com/mttchpmn/ronu) lib for the conversion because of the very short time available

#### additional time
- internet research and reading on nest, vite, svelte, arabic to roman conversion, to prepare dev : ~4h
- pb with the fetch api in front => switch to axios : ~1h
- pb with my gitlab account, as I develop on my work laptop, I had to create a second ssh key and an ssh config : ~30'
- pb dealing with rxjs in backend ~1h

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

```

## Stay in touch

- Author - [Francois Godineau](https://www.linkedin.com/in/francois-godineau-76892225/)
- Website - [Firebase Hosting](https://francois-godineau.web.app/)

## License

Nest is [MIT licensed](LICENSE).