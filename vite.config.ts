import { svelte } from '@sveltejs/vite-plugin-svelte'
import { resolve } from 'path'
import replace from '@rollup/plugin-replace'
import { defineConfig } from 'vite'
// https://vitejs.dev/config/

const defaultConf = {
  plugins: [
    svelte(),
    replace({
      'process.env.ENV': JSON.stringify('dev'),
      'process.env.BACKEND': JSON.stringify('http://localhost:3000'),
    }),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },
}


const productionConf = {
  plugins: [
    svelte(),
    replace({
      'process.env.ENV': JSON.stringify('production'),
      'process.env.BACKEND': JSON.stringify('https://francois-godineau.herokuapp.com'),
    }),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },
}

export default defineConfig(({ command, mode, ssrBuild }) => {
  if (command === 'serve') {
    return defaultConf
  } else {
    // command === 'build'
    return productionConf
  }
})
